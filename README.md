# Projeto Expand JR Plugins

## Instalação

* 1 - Copie as pastas para o seu diretório de arquivos do wp: .../wp-content/plugins/;
* 2 - Após ative os plugins na seção plugins do painel do wp, Slideshow Cases de Sucesso e Slideshow Apoiadores;
* 3 - Com a ativação as seções Apoidores e Cases de Sucesso, aparecerão no painel lateral, nelas você poderá incluir os posts que serão exibidos na home, lembre-se de ao adicionar o post insirir o título para ambos os casos;
* 4 - Fim.