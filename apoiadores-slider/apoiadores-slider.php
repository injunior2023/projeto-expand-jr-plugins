<?php
/*
 * Plugin Name:       Slideshow Apoiadores
 * Plugin URI:        https://www.injunior.com.br
 * Description:       Plugin criado para gerar os sliders do projeto expandjr.
 * Version:           1.10.3
 * Requires at least: 5.2
 * Requires PHP:      7.2
 * Author:            La Marguerita Ggantesca
 * Author URI:        
 * License:           GPL v2 or later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Update URI:        
 * Text Domain:       ap-slider
 * Domain Path:       /languages
 */

//Segurança
if(!defined('ABSPATH')){
    die('Silêncio é ouro!');
    exit;
}

//Criando a classe no Wordpress, if verifica se a classe existe
if(!class_exists('Apoiadores_Slider')){
    class Apoiadores_Slider{
        //construtor
        function __construct(){
            $this->define_constants();
            
            //Adicionando a class do CPT
            require_once(APOIADORES_SLIDER_PATH . 'post-types/class.apoiadores-slider-cpt.php');
            $Apoiadores_Slider_Post_Type = new Apoiadores_Slider_Post_Type();
        }

        public function define_constants(){
            //Caminho pasta plugin
            define('APOIADORES_SLIDER_PATH', plugin_dir_path( __FILE__ ));
            //Endereço web
            define('APOIADORES_SLIDER_URL', plugin_dir_url( __FILE__ ));
            //Versão do Plugin
            define('APOIADORES_SLIDER_VERSION', '1.0.0');
        }

        public static function activate(){
            //Formas de não precisar atualizar os permalinks, update_option funciona melhor, segundo o autor do curso.
            //flush_rewrite_rules();
            update_option( 'rewrite_rules', '');
        }

        public static function deactivate(){
            //Aqui o método funciona bem
            flush_rewrite_rules();
            unregister_post_type( 'apoiadores-slider' );
        }

        public static function uninstall(){

        }
    }
}

if(class_exists('Apoiadores_Slider')){
    register_activation_hook( __FILE__, array('APOIADORES_SLIDER', 'activate'));
    register_deactivation_hook( __FILE__, array('APOIADORES_SLIDER', 'deactivate'));
    register_uninstall_hook( __FILE__, array('APOIADORES_SLIDER', 'uninstall'));
    $apoiadores_slider = new Apoiadores_Slider();
}