<?php

if(!class_exists('Apoiadores_Slider_Post_Type')){
    class Apoiadores_Slider_Post_Type{
        function __construct(){
            add_action( 'init', array($this, 'create_post_type') );
            //Escolhi nomear o nome da metabox igual ao nome do hook, mas pode ser qualquer
            add_action('add_meta_boxes', array($this, 'add_meta_boxes'));
            //Salvando os dados, primeiro save_post é o hook, o segundo é o nome da função ode ser qualquer nome
            add_action('save_post', array($this, 'save_post'), 10, 2);
        }
    
    
        public function create_post_type(){
            register_post_type( 'apoiadores-slider', 
            array(
                'label' => 'Apoiadores Slider',
                'description' =>'Sliders de apoiadores',
                'labels' => array(
                    'name' => 'Apoiadores',
                    'singular_name' => 'slider'
                ),
                'public' =>true,
            'supports' => array('title','editor','thumbnail'/*'page-attributes'*/),
            'hierarchical' => false,
            'show-ui' => true,
            'show_in_menu' => true,
            'menu_position' => 5,
            'show_in_admin_bar' => true,
            'show_in_nav_menus' => true,
            'can_export' => true,
            'has_archive' => false,
            'exclude_from_search' => false,
            'publicly_queryable' => true,
            'show_in_rest' => true,
            'menu_icon' => 'dashicons-images-alt2'
            //'register_meta_box_cb' => array($this, 'add_meta_boxes'); //Método alternativo ao add_action, no construtor
            ) );
        }

        //função para adicionar a metabox
        public function add_meta_boxes(){
            add_meta_box(
                'apoiadores_slider_meta_box',
                'Opções',
                array($this, 'add_inner_meta_boxes'),
                'apoiadores-slider',
                'normal', //'side' --> vai para a janela lateral
                'high',
                //array('foo' => 'bar')
            );
        }

        public function add_inner_meta_boxes($post /*$foobar*/){
            require_once (APOIADORES_SLIDER_PATH . 'views/apoiadores-slider_metabox.php');
        }

        public function save_post($post_id){
            if(isset($_POST['action']) && $_POST['action'] == 'editpost'){
                //resgatando e sanitizando os dados
                $old_img = get_post_meta( $post_id, 'apoiadores_slider_img', true );
                $new_img = sanitize_text_field( $_POST['apoiadores_slider_img'] );

                //$old_link_url = get_post_meta( $post_id, 'apoiadores_slider_link_url', true );
                //$new_link_url = esc_url_raw(  $_POST['apoiadores_slider_link_url'] );

                //usar udate_post_meta, porque se a informação ainda não existir ela tratará como um add
                //echo $new_link_text;
                if(empty($new_img)){
                    update_post_meta( $post_id,'apoiadores_slider_img','http://expandjr.local/wp-content/uploads/2023/03/Capa-Site-Expandindo-seus-Horizontes-scaled-1.jpg', $old_img);
                }else{
                    update_post_meta( $post_id,'apoiadores_slider_img',$new_img, $old_img);
                }
                /*
                if(empty($new_link_url)){
                    update_post_meta( $post_id,'apoiadores_slider_link_url','#', $old_link_url);
                }else{
                    update_post_meta( $post_id,'apoiadores_slider_link_url',$new_link_url, $old_link_url);
                } */               
            }
        }
    }
}
