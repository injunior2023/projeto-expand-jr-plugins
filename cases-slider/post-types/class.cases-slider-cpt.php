<?php

if(!class_exists('Cases_Slider_Post_Type')){
    class Cases_Slider_Post_Type{
        function __construct(){
            add_action( 'init', array($this, 'create_post_type') );
            //Escolhi nomear o nome da metabox igual ao nome do hook, mas pode ser qualquer
            add_action('add_meta_boxes', array($this, 'add_meta_boxes'));
            //Salvando os dados, primeiro save_post é o hook, o segundo é o nome da função ode ser qualquer nome
            add_action('save_post', array($this, 'save_post'), 10, 2);
        }
    
    
        public function create_post_type(){
            register_post_type( 'cases-slider', 
            array(
                'label' => 'Cases Slider',
                'description' =>'Sliders',
                'labels' => array(
                    'name' => 'Cases de Sucesso',
                    'singular_name' => 'slider'
                ),
                'public' =>true,
            'supports' => array('title','editor','thumbnail'/*'page-attributes'*/),
            'hierarchical' => false,
            'show-ui' => true,
            'show_in_menu' => true,
            'menu_position' => 5,
            'show_in_admin_bar' => true,
            'show_in_nav_menus' => true,
            'can_export' => true,
            'has_archive' => false,
            'exclude_from_search' => false,
            'publicly_queryable' => true,
            'show_in_rest' => true,
            'menu_icon' => 'dashicons-images-alt2'
            //'register_meta_box_cb' => array($this, 'add_meta_boxes'); //Método alternativo ao add_action, no construtor
            ) );
        }

        //função para adicionar a metabox
        public function add_meta_boxes(){
            add_meta_box(
                'cases_slider_meta_box',
                'Configurações',
                array($this, 'add_inner_meta_boxes'),
                'cases-slider',
                'normal', //'side' --> vai para a janela lateral
                'high',
                //array('foo' => 'bar')
            );
        }

        public function add_inner_meta_boxes($post /*$foobar*/){
            require_once (CASES_SLIDER_PATH . 'views/cases-slider_metabox.php');
        }

        public function save_post($post_id){
            if(isset($_POST['action']) && $_POST['action'] == 'editpost'){
                //resgatando e sanitizando os dados
                $old_name_client = get_post_meta( $post_id, 'cases_slider_name_client', true );
                $new_name_client = sanitize_text_field( $_POST['cases_slider_name_client'] );
                
                $old_img = get_post_meta( $post_id, 'cases_slider_img', true );
                $new_img = esc_url_raw(  $_POST['cases_slider_img'] );

                $old_description = get_post_meta( $post_id, 'cases_slider_description', true );
                $new_description = sanitize_text_field(  $_POST['cases_slider_description'] );

                //usar udate_post_meta, porque se a informação ainda não existir ela tratará como um add
                if(empty($new_name_client)){
                    update_post_meta( $post_id,'cases_slider_name_client','Add some name', $old_name_client);
                }else{
                    update_post_meta( $post_id,'cases_slider_name_client',$new_name_client, $old_name_client);
                }

                if(empty($new_img)){
                    update_post_meta( $post_id,'cases_slider_img','#', $old_img);
                }else{
                    update_post_meta( $post_id,'cases_slider_img',$new_img, $old_img);
                }

                
                if(empty($new_description)){
                    update_post_meta( $post_id,'cases_slider_description','Add some description', $old_description);
                }else{
                    update_post_meta( $post_id,'cases_slider_description',$new_description, $old_description);
                } 
            }
        }
    }
}
