<?php 
    $meta = get_post_meta( $post->ID);
    //var_dump($meta);
?>
<script>
jQuery(document).ready(function ($) {
  // Instancia a variável que contém o quadro da biblioteca de mídia.
  var meta_image_frame
  // Executa quando o botão de imagem é clicado.
  $('.image-upload').click(function (e) {
    // Obter painel de visualização
    var meta_image_preview = $(this)
      .parent()
      .parent()
      .children('.image-preview')
    // Evita que a ação padrão ocorra.
    e.preventDefault()
    var meta_image = $(this).parent().children('.meta-image')
    // Se o quadro já existir, abra-o novamente.
    if (meta_image_frame) {
      meta_image_frame.open()
      return
    }
    // Configura o quadro da biblioteca de mídia
    meta_image_frame = wp.media.frames.meta_image_frame = wp.media({
      title: meta_image.title,
      button: {
        text: meta_image.button,
      },
    })
    // Executa quando uma imagem é selecionada.
    meta_image_frame.on('select', function () {
      // Pega a seleção de anexo e cria uma representação JSON do modelo.
      var media_attachment = meta_image_frame
        .state()
        .get('selection')
        .first()
        .toJSON()
      // Envia o URL do anexo para nosso campo de entrada de imagem personalizado.
      meta_image.val(media_attachment.url)
      meta_image_preview.children('img').attr('src', media_attachment.url)
    })
    // Abre o quadro da biblioteca de mídia.
    meta_image_frame.open()
  })
})
</script>
<table class="form-table cases-slider-metabox"> 
    <tr>
        <th>
            <label for="cases_slider_description">Descrição</label>
        </th>
        <td>
            <textarea
                type="text" 
                name="cases_slider_description"
                id="cases_slider_description" 
                class="regular-text link-text"
                value="<?php echo isset($meta['cases_slider_description'][0]) ? esc_html($meta['cases_slider_description'][0] ) : ''; ?>"
                
            ><?php echo isset($meta['cases_slider_description'][0]) ? esc_html($meta['cases_slider_description'][0] ) : ''; ?></textarea>
        </td>
    </tr>

    <tr>
        <th>
            <label for="cases_slider_name_client">Nome Cliente</label>
        </th>
        <td>
            <input 
                type="text" 
                name="cases_slider_name_client" 
                id="cases_slider_name_client" 
                class="regular-text"
                value="<?php echo isset($meta['cases_slider_name_client'][0]) ? esc_html($meta['cases_slider_name_client'][0]) : ''; ?>"
                
            >
        </td>
    </tr>  
    
    
    <tr>
        <th>
            <label for="cases_slider_img">Imagem Upload</label>
        </th>
        <td>
            <input 
                type="url" 
                name="cases_slider_img" 
                id="cases_slider_img" 
                class="regular-text link-text meta-image"
                value="<?php echo isset($meta['cases_slider_img'][0]) ? esc_html($meta['cases_slider_img'][0] ) : ''; ?>">

            <input type="button" class="button image-upload" value="Browse">
        </td>
    </tr>
    <tr>
        <td class="image-preview">
            <img src="<?php echo isset($meta['cases_slider_img'][0]) ? esc_html($meta['cases_slider_img'][0] ) : ''; ?>" style="max-width: 250px;">
        </td>
    </tr>

</table>